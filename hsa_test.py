#!/usr/bin/python
"""
    Nick Shelly
    13 Dec 2013
    Stanford CS399: HSA for Open vSwitch Optimized Flow Classification
"""

import termcolor as T
from argparse import ArgumentParser

import sys
import os
import json
from time import time, sleep
from subprocess import Popen

from mininet.net import Mininet
from mininet.node import RemoteController, OVSSwitch
from mininet.log import lg
from mininet.topo import SingleSwitchTopo
from util.helper import stdev
from mininet.cli import CLI

# Margin off error in % for verification
MARGIN = 25 

# Number of samples to skip for reference util calibration.
CALIBRATION_SKIP = 10

# Number of samples to grab for reference util calibration.
CALIBRATION_SAMPLES = 30

# Time for each sample, in seconds, as a float.
SAMPLE_PERIOD_SEC = 10.0

DST_PORT = 50001

CHOKE_IFACE = 's1-eth1'

# High entropy (transport port) range.
ACL_TP_START = 50
ACL_TP_STOP = 20000

def cprint(s, color, cr=True):
    """Print in color
       s: string to print
       color: color to use"""
    if cr:
        print T.colored(s, color)
    else:
        print T.colored(s, color),


# Parse arguments

parser = ArgumentParser(description="HSA flow classification tests")

parser.add_argument('--dir', '-d',
                    dest="dir",
                    action="store",
                    help="Directory to store outputs",
                    default="results",
                    required=True)

parser.add_argument('-n',
                    type=int,
                    action="store",
                    help="Number of nodes in star.  Must be >= 3",
                    required=True)

parser.add_argument('--nparallel',
                    type=int,
                    help="Number of instances of tests run in parallel",
                    required=True)

parser.add_argument('--test',
                    type=str,
                    help="Netperf test",
                    required=True)

parser.add_argument('--hsa_mega_opts',
                    type=str,
                    default="enable_enable,enable_disable,disable_disable",
                    help="HSA and Megaflow options (separated by comma)",
                    required=True)


parser.add_argument('--testlen',
                    type=int,
                    default=SAMPLE_PERIOD_SEC,
                    help="Netperf test duration in seconds",
                    required=True)

parser.add_argument('--resubmits',
                    type=int,
                    default=10,
                    help="OVS table resubmissions before and after match",
                    required=True)


parser.add_argument('--num_acls',
                    type=int,
                    default=100,
                    help="OVS ACLs higher than in matched flow (exponent)",
                    required=True)

# Expt parameters
args = parser.parse_args()

if not os.path.exists(args.dir):
    os.makedirs(args.dir)

lg.setLogLevel('debug')

# File to add flows to
FLOW_FILE = os.path.join(args.dir, 'flows.txt')

#def start_tcpprobe():
#    "Install tcp_probe module and dump to file"
#    os.system("rmmod tcp_probe 2>/dev/null; modprobe tcp_probe;")
#    Popen("cat /proc/net/tcpprobe > %s/tcp_probe.txt" %
#          args.dir, shell=True)
#
#def stop_tcpprobe():
#    os.system("killall -9 cat; rmmod tcp_probe &>/dev/null;")

def avg(s):
    "Compute average of list or string of values"
    if ',' in s:
        lst = [float(f) for f in s.split(',')]
    elif type(s) == str:
        lst = [float(s)]
    elif type(s) == list:
        lst = s
    return sum(lst)/len(lst)

def median(l):
    "Compute median from an unsorted list of values"
    s = sorted(l)
    if len(s) % 2 == 1:
        return s[(len(l) + 1) / 2 - 1]
    else:
        lower = s[len(l) / 2 - 1]
        upper = s[len(l) / 2]
        return float(lower + upper) / 2

# Get a list of the source hosts for this experiment
def get_src_hosts(net):
    host_names = ['h%d' % i for i in range(1, args.n)]
    src_hosts = net.getNodeByName(*host_names)
    return src_hosts

# Start iperf on the receiver node
# Hint: use getNodeByName to get a handle on the sender node
# Hint: iperf command to start the receiver:
#       '%s -s -p %s > %s/iperf_server.txt' %
#        (CUSTOM_IPERF_PATH, 5001, args.dir)
# Note: The output file should be <args.dir>/iperf_server.txt
#       It will be used later in count_connections()

def start_receiver(net):
    server = net.getNodeByName('h2')
    print "Starting receiver on %s" % server.name 
    sys.stdout.flush()
    for n in range(args.nparallel):
        cmd_str = "netserver -p %d &" % (DST_PORT+n)
        server.cmd(cmd_str)

# Start args.nparallel netperf tests against server
# Use getNodeByName to get a handle on the sender (A or B in the
# figure) and receiver node (C in the figure).

def start_senders(net, hsa_mega_opt):
    host = net.getNodeByName('h1')
    server = net.getNodeByName('h2')
    #src_hosts = get_src_hosts(net)
    print "Starting %d flows from %d hosts to %s" % \
        (args.nparallel, args.n-1, server.name)
    for n in range(args.nparallel):
        #h = src_hosts[n % (args.n - 1)]
        output_file = "sender%s-%d_%s.txt" % (1, n, hsa_mega_opt)
        #print "Starting flow %d from %s to %s" % (n, h.name, server.name)
        if args.testlen > 0:
            len_str = "-l %d" % args.testlen
        else:
            len_str = ""

        cmd_str = "netperf -H %s -t %s -p %d %s > %s/%s &" % \
                  (server.IP(), args.test, DST_PORT+n, len_str, args.dir, 
                   output_file)
        host.cmd(cmd_str)

def add_flow(switch, flow_str, ffile):
    if ffile:
        ffile.write("%s\n" % flow_str)
    else:
        cmd_str = "ovs-ofctl add-flow %s \"%s\"" % (switch.name, flow_str)
        cprint(cmd_str, "cyan")
        switch.cmd(cmd_str)

def add_flows(net, resubmits):

    flowf = open(os.path.join(args.dir, 'flows.txt'), 'w+')
    
    switch = net.getNodeByName('s1')

    host = net.getNodeByName('h1')
    host_port = 1

    server = net.getNodeByName('h2')
    server_port = 2

    print "Adding flows for %d resubmits, from h%d to server" % \
            (args.resubmits, 1)

    (src, src_port) = (host, host_port)
    (dst, dst_port) = (server, server_port)

    # Start with highest priority
    priority = (1 << 16) - 1

    START_RESUBMIT_PORT = 10
    in_port = START_RESUBMIT_PORT

    add_flow(switch, "table=0,priority=%d,arp,actions=%d,%d" %
                     (priority, host_port, server_port), flowf)
    priority -= 1

    # Add first part of resubmissions.
    for i in range(args.resubmits):
        if i < args.resubmits - 1:
            next_table_no = 0
        else:
            next_table_no = 1
        add_flow(switch, "table=0,priority=%d,in_port=%d," \
                         "actions=resubmit(%d,%d)" % \
                            (priority, in_port, in_port+1, next_table_no), 
                         flowf)
        in_port += 1

    # Add second part of resubmissions.
    in_port = START_RESUBMIT_PORT + args.resubmits + 1
    for i in range(args.resubmits-1):
        add_flow(switch, "table=2,priority=%d,in_port=%d,"
                         "actions=resubmit(%d,%d)" % \
                            (priority, in_port, in_port+1, 2),
                         flowf)
        in_port += 1

    if args.resubmits > 0:
        # Add ACLs to middle of resubmissions
        acl_table = 1
        acl_in_port = START_RESUBMIT_PORT+args.resubmits
    else:
        # Add ACLs to simple in port
        acl_table = 0
        acl_in_port = 1

    for i in range(args.num_acls):
        tp_dst = (ACL_TP_START + i) % (ACL_TP_STOP - ACL_TP_START)
        add_flow(switch, "table=%s,priority=%d,in_port=%d,"
                         "tcp,tp_dst=%d,actions=drop" % \
                            (acl_table, priority, 
                             acl_in_port, tp_dst),
                         flowf)
    priority -= 1000

    if args.resubmits > 0:
        # Add default rule (use optimal megaflows for).
        add_flow(switch, "table=1,priority=%d,in_port=%d,"
                         "tcp,actions=resubmit(%d,2)" % \
                            (10000, START_RESUBMIT_PORT+args.resubmits,
                             START_RESUBMIT_PORT+args.resubmits+1), flowf) 

        priority -= 1000

    for direction in range(2):
        if direction:
            (src, src_port) = (host, host_port)
            (dst, dst_port) = (server, server_port)
        else:
            (src, src_port) = (server, server_port)
            (dst, dst_port) = (host, host_port)
                    

        if args.resubmits > 0:
            # Add an initial flows, which sends to series of resubmits
            add_flow(switch, "table=0,priority=%d,in_port=%d,"
                             "actions=resubmit(10,0)" % \
                                (priority, src_port), flowf)
                    
            # Add final flow, for the packet to leave the switch.
            add_flow(switch, "table=2,priority=%d,in_port=%d,"
                             "dl_src=%s,actions=output:%d" % \
                                (priority, START_RESUBMIT_PORT+2*args.resubmits,
                                 src.MAC(), dst_port), flowf)
        else:
            add_flow(switch, "table=0,priority=%d,in_port=%d,"
                             "dl_src=%s,actions=output:%d" % \
                                (priority, src_port, src.MAC(), dst_port), 
                             flowf)

    flowf.close()
    switch.dpctl("add-flows", FLOW_FILE)

    switch.dpctl("dump-aggregate")

def set_megaflow_option(net, hsa_mega_opt):
    switch = net.getNodeByName('s1')

    (hsa_opt, mega_opt) = hsa_mega_opt.split('_')
    cmd_str = "ovs-appctl dpif/%s-hsa" % hsa_opt
    cprint(cmd_str, "cyan")
    switch.cmd(cmd_str)

    cmd_str = "ovs-appctl upcall/%s-megaflows" % mega_opt
    cprint(cmd_str, "cyan")
    switch.cmd(cmd_str)

def main():
    "Create network and run HSA Flow experiment"

    start = time() # Reset to known state
    #topo = StarTopo(args.n)
    topo = SingleSwitchTopo(args.n)

    controller = lambda name: RemoteController( name, ip='127.0.0.1' )
    net = Mininet(topo=topo, autoSetMacs=True, switch=OVSSwitch, \
                  controller=controller, listenPort=6634)
    #net = Mininet(topo=topo, autoSetMacs=True)
    net.start()
    #dumpNodeConnections(net.hosts)
    #net.pingAll()

    results = {}
    # Verify latency and bandwidth of links in the topology
    #verify_latency(net)
    #verify_bandwidth(net, seconds=20)
    add_flows(net, args.resubmits)

    # In the format HSA_MEGAFLOWS, so "enable_disable" means HSA enabled and 
    #   Meagflows disabled
    cprint("args.hsa_mega_opts = %s" % args.hsa_mega_opts.split(','), "green")
    for hsa_mega_opt in args.hsa_mega_opts.split(','):
        set_megaflow_option(net, hsa_mega_opt)

        start_receiver(net)

        # Wait a bit for the receivers to all start up.
        sleep(1)
        cprint("Starting experiment: HSA: %sd, Megaflows: %sd, "
               "ACLs: %d, Resubmits: %d" % \
                (hsa_mega_opt.split('_')[0], hsa_mega_opt.split('_')[1],
                 args.num_acls, args.resubmits), "green")

        start_senders(net, hsa_mega_opt)

        print "Waiting for netperf processes to finish..." 

        # Wait for netperf's to finish
        Popen("while [ \"`pidof netperf`\" ] ; do sleep 1 ; done; killall netserver", 
              shell=True).wait()

        Popen("while [ \"`pidof netserver`\" ] ; do sleep 1 ; done; killall netserver", 
              shell=True).wait()

        ## Store output.  It will be parsed by run.sh after the entire
        ## sweep is completed. 
        avgs = []
        total = 0.0

        if args.test == "TCP_CRR":
            offset = -3
        elif args.test == "TCP_STREAM":
            offset = -2
        for fname in os.listdir(args.dir):
            if hsa_mega_opt in fname:
                f = open(os.path.join(args.dir, fname))
                text = f.read()
                try:
                    print text.split('\n')[offset].split()
                    new_avg = float(text.split('\n')[offset].split()[-1])

                    total += new_avg

                    #print "new average: %f" % new_avg
                    avgs.append(new_avg)
                except Exception, e:
                    print "Invalid netperf session", e
                f = open(os.path.join(args.dir, fname))
                cprint(f.read(), "green")

        if (len(avgs) != args.nparallel):
            "Some flows didn't launch (%d/%d)!" % \
                (len(avgs), args.nparallel)

        result = {}
        result["n"] = len(avgs)
        result["total"] = total
        print "avgs", avgs
        result["avg"] = avg(avgs)
        result["median"] = median(avgs)
        result["stdev"] = stdev(avgs)
        results[hsa_mega_opt] = result 

    results["resubmits"] = args.resubmits
    results["num_acls"] = args.num_acls
    results["nparallel"] = args.nparallel
    results["testlen"] = args.testlen
        
    f = open("%s/result.txt" % args.dir, "w")
    json_string = json.dumps(results, indent=4)
    f.write(json_string)
    print "results: %s" % json_string

    net.stop()
    Popen("killall -9 top bwm-ng tcpdump cat mnexec netserver netperf &>/dev/null", 
           shell=True).wait()
    end = time()
    cprint("Sweep took %.3f seconds" % (end - start), "yellow")

if __name__ == '__main__':
    lg.setLogLevel('debug')
    try:
        main()
    except:
        raise
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top bwm-ng tcpdump cat mnexec iperf &>/dev/null; mn -c")
