#!/usr/bin/env python

import os
import json
import glob
import argparse
from collections import defaultdict
import matplotlib
matplotlib.use('Agg')

from pylab import *

parser = argparse.ArgumentParser()

parser.add_argument('-o', '--out',
                    help="Save plot to output file, e.g.: --out plot.png",
                    dest="out",
                    required=True)

parser.add_argument('--dir',
                    dest="dir",
                    help="Directory from which outputs of the sweep are read.",
                    required=True)

args = parser.parse_args()
data = defaultdict(list)

print args.out
if not os.path.exists(args.out):
    os.makedirs(args.out)

def first(lst):
    return map(lambda e: e[0], lst)

def second(lst):
    return map(lambda e: e[1], lst)

def avg(lst):
    return sum(lst)/len(lst)

def median(lst):
    l = len(lst)
    lst.sort()
    return lst[l/2]

nfiles = 0

max_totals = []
#tests = ["enable_enable", "disable_enable", "disable_disable", 
#         "disable_disable"]
tests = ["enable_enable", "disable_disable"]
graph = ["go-","ro-","gs--","rs--"]
for plot_resubmits in [0, 15, 30]:
    
    totals = {}
    tdict = {}
    for t in tests:
        totals[t] = []
        tdict[t] = {}

    for f in glob.glob("%s/*/result.txt" % args.dir):
        json_data = open(f)
        d = json.load(json_data)

        if d["resubmits"] != plot_resubmits:
            print "Skipping test: %s" % t
            continue

        print "Parsing %s" % f
        print d
        for t in tests:
            if t not in d:
                print "Skipping test: %s" % t
                continue

            assert abs(d[t]['n'] * d[t]['avg']) - d[t]['total'] < 0.01
        
            if str(d["num_acls"]) not in tdict[t]:
                tdict[t][str(d["num_acls"])] = []

            tdict[t][str(d["num_acls"])].append(d[t]["total"])

    print "plot_resubmits =", plot_resubmits
    for t in tests:
        for n in sorted(tdict[t].keys()):
            totals[t].append((int(n), sum(tdict[t][n])/len(tdict[t][n])))

        totals[t].sort()
        print t, totals[t]


    # Plot average degree on 2d-graph 
    fig = figure()

    xlabel('# higher priority rules (e.g. ACLs)')
    ylabel('Transactions per second')
    title('Flow Classifier Comparison - Performance (hops=%d)' % plot_resubmits)

    for t in tests:
        
        hsa, mega = t.split('_')
        label = ""
        if hsa == "enable":
            label += "HSA Optimized, "
        else:
            label += "Non-HSA, "
        label += "Megaflows %sd" % mega

        if len(totals[t]) > 0:
            print "Plotting %s" % t
            loglog(first(totals[t]), second(totals[t]), label='%s' % label)

    legend(loc='upper right')

    pname = os.path.join(args.out, 'HSA_r%d.png' % (plot_resubmits))
    print "Saving figure %s" % pname
    savefig(pname)

    # Get peak transactions per second reached.
    if plot_resubmits == 0:
        print "Updating maximum values."
        for i in range(len(totals["enable_enable"])):
            mtotals = [totals[t][i][1] if len(totals[t]) > 0 else [] for t in tests]
            print "mtotals = ", mtotals
            max_totals.append(max(mtotals))
        print "max_totals = ", max_totals

    # No maximums set, skip graph.
    if not max_totals:
        continue


    # Get the percentage of total utilization
    percent = {}
    for t in tests:
        percent[t] = []

    for i, m in enumerate(max_totals):
        for test in tests:
            if len(totals[test]) > 0:
                percent[test].append(totals[test][i][1] / m)

    print "percent", percent
    figure()

    for t, g in zip(tests, graph):
        i = 0
        hsa, mega = t.split('_')
        label = ""
        if hsa == "enable":
            label += "HSA Optimized, "
        else:
            label += "Non-HSA, "
        label += "Megaflows %sd" % mega

        if len(totals[t]) > 0:
            print "Plotting %s" % t
            plot(first(totals[tests[0]]), percent[t], label='%s' % label)
            draw()
    grid()
    xscale('log')
    xlabel('# higher priority rules (e.g. ACLs)')
    ylabel('% of peak transactions per second')
    ylim(0, 1.1)
    title('Flow Classifier Comparison - Link Utilization (hops=%d)' % plot_resubmits)

    pname = os.path.join(args.out, 'HSA_utilization_r%d.png' % (plot_resubmits))
    print "Saving figure %s" % pname
    savefig(pname)
